import {StatusBar} from 'expo-status-bar';
import {StyleSheet, Text, View, SafeAreaView, Button, Alert, TouchableHighlight} from 'react-native';
import * as expoLocalAuthentication from "expo-local-authentication";
import React, {useState, useEffect} from "react";


export default function App() {

    const [isBiometricSupported, setBiometricSupported] = useState(false)

    useEffect(() => {
        (async () => {
            const compatible = await expoLocalAuthentication.hasHardwareAsync();
            setBiometricSupported(compatible)
        })();
    })

    const fallBackToDefault = () => {
        console.log('fallback to password')
    }

    const alertComponent = (title, message, btnText, btnFunction) => {
        return Alert.alert(title, message, [
            {text: btnText, onPress: btnFunction}
        ])
    }

    const twoButtonAlert = () => {
        Alert.alert('Welcome to app', 'authorized', [
            {text: 'Back', onPress: () => console.log('Cancel pressed'), style: 'cancel'},
            {text: 'Ok', onPress: () => console.log('ok pressed')}
        ])
    }

    const handleBiometricAuth = async () => {
        const isBiometricAvailable = await expoLocalAuthentication.hasHardwareAsync();

        if (!isBiometricAvailable)
            return alertComponent(
                'please enter your password',
                'biometric is not supported',
                'ok',
                () => fallBackToDefault()
            )

        let supportedBiometrics;
        if (isBiometricAvailable)
            supportedBiometrics = await expoLocalAuthentication.supportedAuthenticationTypesAsync();

        const saveBiometrics = await expoLocalAuthentication.isEnrolledAsync();
        if (!saveBiometrics)
            return alertComponent(
                'Biometric record not found',
                'Please Login with password',
                'ok',
                () => fallBackToDefault()
            )

        const biometricAuth = await expoLocalAuthentication.authenticateAsync({
            promptMessage: 'Login with biometrics',
            cancelLabel: 'cancel',
            disableDeviceFallback: false
        })

        if (biometricAuth) {twoButtonAlert()}
        console.log({biometricAuth})

    }

    return (
        <SafeAreaView>
            <View style={styles.container}>
                <Text>
                    {
                        isBiometricSupported ? 'your device is compatible with biometrics' :
                        'Biometrics not available'
                    }
                </Text>
                <TouchableHighlight style={{ height: 60, width: 200 }}>
                    <Button
                        title='Login with biometrics'
                        color='black'
                        onPress={handleBiometricAuth}
                    />
                </TouchableHighlight>
                <StatusBar style='auto'/>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 50,
    },
});
